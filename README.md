# Wordsmith

Wordsmith is a conlang construction toolkit.  

Wordsmith is a fork of the project [PolyGlot](https://github.com/DraqueT/PolyGlot).

For full user documentation, see: http://draquet.github.io/PolyGlot/readme.html or the readme file included in the docs section of this project.

For the project roadmap, see the todo file.

Instructions for building this project can be found in docs/readme_developers.txt.

Wordsmith uses the MPL-2.0 license. Wordsmith is based on the code of [PolyGlot](https://github.com/DraqueT/PolyGlot), which uses the MIT license. The license can be found at LICENSE.txt, and the PolyGlot license can be found at LICENSE.polyglot.txt.
